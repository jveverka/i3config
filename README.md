# Overview

My configs for i3 wm, most of ideas gathered from http://i3wm.org/docs/user-contributed/lzap-config.html

# Dependencies

## In my configs are used following dependencies (Fedora 20 packages):
 - i3
 - i3status
 - i3lock
 - dunst
 - xbacklight
 - pinta
 - scrot
## Nice to have:
 - network-manager-applet
 - nm-connection-editor

# Tips

## Place program to run on specific desktop (screen)
You will need to know X window name in order to force program on desired desktop (screen).
I am using xprop to get info. You will get following output
```
-> % xprop
WM_STATE(WM_STATE):
                window state: Normal
                icon window: 0x0
_NET_WM_PID(CARDINAL) = 4637
WM_PROTOCOLS(ATOM): protocols  WM_DELETE_WINDOW, _NET_WM_PING
WM_LOCALE_NAME(STRING) = "en_US.UTF-8"
WM_CLASS(STRING) = "urxvt", "URxvt"
WM_HINTS(WM_HINTS):
                Client accepts input or input focus: True
                Initial state is Normal State.
                window id # of group leader: 0xa0000a
WM_NORMAL_HINTS(WM_SIZE_HINTS):
                program specified minimum size: 13 by 21
                program specified resize increment: 7 by 15
                program specified base size: 6 by 6
                window gravity: NorthWest
WM_CLIENT_MACHINE(STRING) = "localhost.localdomain"
WM_COMMAND(STRING) = { "urxvt", "-e", "tmux" }
_NET_WM_ICON_NAME(UTF8_STRING) = "tmux"
WM_ICON_NAME(STRING) = "tmux"
_NET_WM_NAME(UTF8_STRING) = "tmux"
WM_NAME(STRING) = "tmux"
```

From this window name I need is at line "WM_CLASS(STRING)".

If I want my terminal to allways start on screen 5, I will have to add following line to my config:
```
assign [class="^urxvt*"] 5
```
## Set sink to manage your volume
pactl utility can be used define your output device:
```
-> % pactl list short sinks
0       alsa_output.pci-0000_00_03.0.hdmi-stereo        module-alsa-card.c      s16le 2ch 44100Hz       SUSPENDED
1       alsa_output.pci-0000_00_1b.0.analog-stereo      module-alsa-card.c      s16le 2ch 44100Hz       RUNNING
```

I am using option 1, so I have to specify SINK number 1 in line
```
bindsym XF86AudioLowerVolume exec /usr/bin/pactl set-sink-volume 1 -- '-5%'
```
